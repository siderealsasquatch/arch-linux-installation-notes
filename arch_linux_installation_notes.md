# My Arch Linux installation notes

These notes detail the steps that I took to dual boot Arch Linux on my main rig. My
current storage setup consists of two SSDs: one containing a Windows 10 installation and
the other containing an Arch Linux installation. Additionally, I have an HDD containing my
personal files. The plan is to give both OSes their own EFI partition so that the
bootloaders don't interfere with each other. This has the added benefit of allowing me to
boot directly into either OS via the BIOS.

## Installation

### Preliminary stuff

Obviously, I'm going to need a bootable drive with the latest Arch ISO on it. I'll also
need to make sure that UEFI is enabled and that Fast Boot and Secure Boot are disabled.

#### Enable UEFI

This should already be enabled in your mobo but it never hurts to check again. Access your
mobo's bios and search for a UEFI option. It should say that it's enabled. If it's not
then you'll need to enable it and potentially reinstall whatever OS is already installed
on your system.

#### Disable Fastboot

On the Windows 10 OS side:

- Go to the power options in the control panel of Windows and select the option in the
  left pane that says something like "Choose what to do when the power button is pressed"
- Select the option to show the advanced settings and make sure that the "Fast boot"
  option isn't selected.

On the motherboard side:

- In the motherboard's UEFI bios, see if there is a fast boot option. It should be in the
  "Boot" tab.
- If there is a fast boot option, make sure to disable it. I know for sure there's one in
  my ASUS ROG Strix B450-F Gaming mobo.

#### Disable Secure boot

- Access your mobo's UEFI bios.
- You'll need to root around for a secure boot option and disable it. This might entail
  selecting "Other OS" from a drop down menu.

### Check if I'm in EFI mode

Run the following command:

```
efivar -l
```
If I see a list of variables then I know I'm in UEFI mode. If I don't, I'll need to make
sure that I've enabled UEFI mode.

### Partitioning the hard drive

This step is practically automatic for me. I just need to remind myself of a few things:
- Use either `cfdisk` or `cgdisk`, preferrably `cgdisk` since I'm working with a GPT
  partition table.
- In the event that I decide to go with separate EFI partitions, create another EFI
  partition and format it as a FAT32 filesystem. Make sure that the EFI partition is
  around 500 MB.
- Create a swap partition the same size as your RAM to allow for hibernation/suspension.
- The remaining space should be used for the root partition.

Once the partition table has been written, don't forget to create the filesystems on the
partitions:

```
# For EFI partition
mkfs.fat -F32 /dev/sdXN

# For Swap
mkswap /dev/sdXN
swapon /dev/sdXN

# For everything else
mkfs.ext4 /dev/sdXN
```

Now mount them to `/mnt`.

```
# Mount /home
mount /dev/sdXN /mnt

# Mount EFI partition
mkdir -p /mnt/boot/efi
mount /dev/sdXN /mnt/boot/efi
```

### Setting up mirrorlist

Here I'll use `reflector` to get the fastest mirrors relative to my location. Check
`/etc/pacman.d/mirrorlist` to see if the mirrors have been commented out. In the latest
ISO as of this writing (2020-12) they shouldn't be but if they are uncomment them with
`sed` while simultaneously creating a backup:

```
sed 's/^#\(Server\)/\1/' /etc/pacman.d/mirrorlist.bak
```

Again, even if they aren't commented out be sure to make a backup of the mirrorlist. Then
rank the mirrors:

```
reflector -c Indonesia,Malaysia,Thailand,Vietnam,Australia,"New Zealand",Taiwan -l 20
--sort rate --save /etc/pacman.d/mirrorlist
```

### Installing Arch Linux base files

Run the following command to install the base files:

```
pacstrap -i /mnt base{,-devel}
```

### Chroot into `/mnt`

First, generate the fstab file

```
genfstab -U -p /mnt >> /mnt/etc/fstab
```

Then check to see if there are entries for the partitions you mounted. Edit the file to
include any missing partitions:

```
# Install neovim first since the Arch Linux base installation doesn't come installed with
# an editor
pacman -S neovim

nvim /mnt/etc/fstab
```

Chroot into `/mnt`

```
arch-chroot /mnt
```

### Set language

Uncomment your locale in the `locale.gen` file. For me, it's `en_US.UTF-8`

```
nvim /etc/locale.gen
```

Generate the locale and then set the language

```
# Generate locale
locale-gen

# Set the language
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
```

### Set time

Set the time by creating a symlink to our timezone. In my case its:

```
ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
```

Then set the hardware clock:

```
hwclock --systohc --utc
```

### Set hostname

Set the system hostname with:

```
echo poodonkis > /etc/hostname
```

Replace "poodonkis" with whatever other hostname you prefer

### Enable multilib repository and pacman color output

In `/etc/pacman.conf`, uncomment the lines with "multilib" and "color" and then update
with

```
pacman -Syu
```

### Setup users

Create a user for yourself with the following:

```
# Might as well install zsh here and set it as the default shell to save us some trouble
pacman -S zsh
useradd -mg users -G wheel,storage,power -s /bin/zsh fahmi
passwd fahmi
```

Where you can replace `fahmi` with anything.

### Setup sudoers

Here I allow all members of the "wheel" group to use `sudo` with the following command:

```
EDITOR=nvim visudo
```

and then uncomment

```
%wheel ALL=(ALL) ALL
```

### Install grub

Install the necessary grub packages with

```
pacman -S grub efibootmgr os-prober
```

Then run

```
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=arch_grub --recheck
os-prober
grub-mkconfig -o /boot/grub/grub.cfg
```

## Reboot prep

Before I reboot, I'll install some packages to set up a working desktop environment. Well,
as much of a desktop environment that can be made out of i3.

### Network

I want to make sure that I have a way to painlessly connect to either a wireless or wired
connection when I reboot. Network Manager is the best solution in my case. I'll also
install the network mananger applet for easy access to networking options.

```
# Install the Network Manger packages
pacman -S networkmanager network-manager-applet

# Disable dhcpcd before enabling the service
systemctl disable dhcpcd # System may report that this service doesn't exist.
systemctl enable NetworkManager
```

### Graphical component

Next, I'll install all of the packages necessary for a functional graphical session. I'll
start with `xorg`:

```
pacman -S xorg{,-{xinit,twm,xclock}} xterm
```

Test that it works by running `startx`. If you see a bunch of terminals and a clock then
`xorg` has been correctly installed. I'll install `mesa` along with the 32-bit libraries
for steam compatibility:

```
pacman -S mesa lib32-mesa
```

After that I'll install the last set of graphics related packages: the nvidia drivers

```
pacman -S nvidia{,-libgl,-settings,-utils} libglvnd lib32-{nvidia-utils,libglvnd}
```

I'll then need to enable persistence for the driver to work properly:

```
systemctl enable nvidia-persistenced
```

### i3 packages

Now I'll finally install the i3 packages

```
pacman -S i3
```

In the resulting menu simply press enter to install all of the packages in the `i3` group.
By default, the `i3wm` package won't be installed in favor of `i3-gaps` which is what we
want.

### lightdm

Now I'll need to install some sort of session manager. I've used `lightdm` before in the
past and never recalled having problems with it so I'll do so again.

```
pacman -S lightdm{,-gtk-greeter{,-settings}}
```

Then I'll enable the service

```
systemctl enable lightdm
```

### Terminal

For my terminal emulator, I'm using `kitty`:

```
pacman -S kitty
```

### Image viewers

I'll install two: feh for setting the background and viewnior for casual image viewing:

```
pacman -S feh viewnior
```

### File manager

Again, I'll install two: ranger for browsing in the terminal and Thunar for managing
external drives:

```
pacman -S ranger thunar{,-volman} gvfs
```

### Text editor

I've already installed my preferred text editor `neovim` but now I'll install certain
complementary packages as well as an actual IDE that I've grown to like (VSCode):

```
pacman -S python{,-{pip,neovim,pywal}} cmake code
```

The `python-pywal` package isn't really necessary for neovim but I will need it later for
customizing i3.

### Browser

Here I'll install firefox:

```
pacman -S firefox
```

### pdf viewer

Specifically for i3 I've decided to use zathura to read my pdf and djvu files:

```
pacman -S zathura{,-{djvu,-pdf-mupdf}}
```

### Miscellaneous utilities

Here are the rest of the packages to round out the reboot prep:

```
pacman -S git openssh stow xclip xdotool maim rofi alsa-utils asoundconf lxappearance \
  xcape file-roller w3m
```

## Post-installation

Now I can finally finish the last bit of configuration. The first thing I'll do is finish
the i3 setup wizard that I'll be greeted with upon rebooting. I'll then set up ssh keys
for my GitHub account for easy access to my dotfiles and install an AUR helper. Afterwards
I'll install some command line tools that I've grown accustomed to using and are thus part
of my environment. I'll also install `oh-my-zsh` and some themes that I like to use.
Finally, I'll set up sound and Steam.

### stowing my dotfiles

I say stow here because I use the stow utility to manage my dotfiles. First thing's first.
Create ssh keys for my siderealsasquatch account as well as the usual github-squatch alias
in the ssh config file. In case I forget:

```
mkdir .ssh
ssh-keygen -t rsa -b 4096 -f ~/.ssh/github-squatch-key -N ""
xclip -sel clip < ~/.ssh/github-squatch-key.pub
```

Then setup the config file with `touch ~/.ssh/config` and enter the following:

```
Host github-squatch
  User git
  Hostname github.com
  IdentityFile ~/.ssh/github-squatch-key
  IdentitiesOnly yes
```
And then paste the key into the appropriate field on my github account settings for
managing public keys. Test the connection with:

```
ssh -T github-squatch
```

Now I can clone my dotfile repo:

```
git clone github-squatch:siderealsasquatch/dotfiles.git
```

Then `cd` into it, switch to the `main-i3` branch, and run the `stow_all` script.

### Installing `oh-my-zsh`

The first thing I'll need to do is check to see if `curl` is installed:

```
which curl
```

If I don't see `/usr/bin/curl` then I'll need to install curl with:

```
sudo pacman -S curl
```

Once this is done, I'll need to run the following command:

```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

This will essentially clone the `oh-my-zsh` repo to `~/.oh-my-zsh` and run you through
a terminal setup wizard. Once you finish the installation, you'll have a template `.zshrc`
file for use with `oh-my-zsh` as well as either a backup of your stowed `.zshrc` file or
just the symlink. Delete this `.zshrc` and the backup if one exists. You may even have to
restore the symlink. After you've done this, we'll install two themes for `zsh`. For the
first theme, run the following command:

```
git clone github-squatch:romkatv/powerlevel10k $ZSH_CUSTOM/themes/powerlevel10k
```

And then make sure that `zsh` is set to use this theme by opening your `.zshrc` file and
checking for the following line:

```
ZSH_THEME="powerlevel10k/powerlevel10k"
```

For the second theme, run the following commands:

```
# Clone the repo to the themes subdirectory of you 'oh-my-zsh' installation
git clone github-squatch:denysdovhan/spaceship-prompt "$ZSH_CUSTOM/themes/spaceship-prompt"

# Symlink the theme to the themes subdirectory
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
```

Then, if you want to use this theme set zsh to use it with:

```
ZSH_THEME="spaceship"
```

You can now either source your `.zshrc` file or close and then open a new terminal for the
changes to take effect.

### Installing yay

I've taken to using `yay`. To install yay

```
mkdir builds
cd builds
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -sri
```

From here I might as well install some necessary fonts and `compton-tryone-git` to finish
up my i3 configuration

```
yay --aur -S compton-tryone-git ttf-font-awesome nerd-fonts-complete \
  ttf-nerd-fonts-symbols adobe-source-han-sans-otc-fonts ttf-twemoji-color
```

If I haven't already done so, I should restart my i3 session.

### Setting up sound

Use `alsamixer` to make sure that every channel in the "Generic" sound card is unmuted
(use the M key to mute/unmute channels).  Then list the available sound cards with
`asoundconf list` and set the default one with `asoundconf set-default-card <card_name>`.
Play a youtube video to make sure that sound is working. Once this is done install pulse
audio along with a helpful applet:

```
sudo pacman -S pulseaudio{,-alsa}
yay -S pa-applet-git
```

### Installing required command line tools

These are the tools that I've integrated into my general environment. My setup wouldn't
function properly without them:

```
# Tools found in the pacman repos
sudo pacman -S ripgrep fd fzf bat jq

# Tools found in the AUR
yay -S lazygit git-delta-bin
```

### Setting up power management

Here I'll cheat a bit and install an existing power management program. I've taken to
installing `xfce4-power-manager` since I've found that it actually works with my setup.

```
sudo pacman -S xfce4-power-manager
```

Once installed, start `xfce4-power-manger` as a daemon process by entering
`xfce4-power-manger` in a terminal. From there, open the `xfce4-power-manager` settings
through `rofi` (search for `xfce4-power-management-settings`). In the floating window that
appears, make sure that the following options are set:

- In the general tab:
  + In the 'Buttons' section, set 'When power button is pressed' to 'Ask'
  + Make sure that everything else in the 'Buttons' section is set to 'Do Nothing'
- In the system tab:
  + In the 'System power saving' section make sure that 'System sleep mode' is set to
    'Suspend' and that 'When inactive for' is set to '30 minutes' (move the slider)
  + In the 'Security' section, make sure that 'Lock system when system is going to sleep'
    is checked.
- In the display tab:
  + Make sure that 'Display power management' is enabled
  + Set 'blank after' to 10 minutes
  + Set 'put to sleep after' to 15 minutes
  + Set 'switch off after' to 20 minutes

Once all of the power management stuff is setup add the following
line to your `i3` config file:

```
# Start xfce4-power-manager
exec --no-startup-id xfce4-power-manager
```

### Setting up a lockscreen

To protect your current session, it would be prudent to install a lockscreen. I've chosen
to go with `betterlockscreen`. Since it's an AUR package, you'll need to install it with
`yay`:

```
yay -S betterlockscreen
```

Once installed, update the background image with:

```
betterlockscreen -u /path/to/background/image
```

and then add a keyboard shortcut to lock the screen to your `i3` config file. Mine is:

```
bindsym $mod+Shift+z exec betterlockscreen -l
```

You'll also want to make it so that your lockscreen activates when your monitor is turned
off by `mate-power-manager`. The easiest way that I've found is to use `xss-lock`:

```
sudo pacman -S xss-lock
```

and then add the following line to your `i3` config to start `xss-lock` on startup:

```
exec --no-startup-id xss-lock -- betterlockscreen -l dim
```

### Enabling automatic syncing of system time to NTP servers

Systemd uses the NTP service to ensure that the system clock displays the proper local
time. By default this service is disabled. In order to enable it, run the following
command:

```
timedatectl set-ntp true
```

Note that you'll be prompted to input your password. You can check that the NTP service
has been enabled with the following command:

```
timedatectl status
```

The `NTP service` entry should be set to `active`.

### Installing steam

The final thing to do is to install steam:

```
# Will also need to install `ttf-liberation` to get around errors with the font
pacman -S ttf-liberation
sudo pacman -S steam
```
