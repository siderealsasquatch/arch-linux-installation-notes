# Some miscellaneous post-installation configuration

## Aesthetic stuff

### Using `pywal`

If you followed the instructions in the Arch Linux installation notes then you should have
`pywal` already set up. The only thing to do now is to actually have `pywal` sync the
colors of your environment to the current wallpaper. This is easily done by entering the
following command:

```
wal -i "<path/to/img>"
```

`pywal` will create a colorscheme based on the wallpaper and automatically apply it to
your terminal. In order to have these changes apply to all of your subsequent terminal
sessions, you'll need to add the following line to your `.zshrc` file:

```
(cat ~/.cache/wal/sequences &)
```

In your case, this should already be in your `.zshrc` file. This is here for future
reference. In order to have the colorscheme persist upon reboot, you'll need to add the
following line to your i3 config:

```
exec --no-startup-id wal -R
```

Again, this should already be in your i3 config.

### Have GTK theme match `pywal` theme

In order to have the theme of any GTK application match the current `pywal` theme, you'll
need to use the `warnai` script. Create `~/src` and `~/bin` directories if you haven't
already done so. In the `~/src` directory, run the following commands:

```
git clone github-squatch:reorr/warnai.git

# Wait for the repo to finish cloning before entering the next line
ln -s warnai/warnai ~/bin
```

Afterwards, install the `inkscape` and `optipng` packages and create a `warna` dir in the
`.themes` dir (should be in ~):

```
# Create `warna` dir. May want to choose a more descriptive name though.
mkdir -p .themes/warna
sudo pacman -S inkscape optipng
```

Once this is done, simply run:

```
warnai -w -g fantome
```

to create a GTK theme called `warna` that's based on the current `pywal` colorscheme.
To apply this theme, open `lxappearance` and select the `warna` theme.

### Matching `rofi` color theme with pywal

Create a rofi config file (rofi doesn't do this by default)

```
mkdir -p .config/rofi
touch .config/rofi/config
```

Then add the following lines. Comment out the one you don't want

```
# Dark theme
rofi.theme: ~/.cache/wal/colors-rofi-dark.rasi

# Light theme
rofi.theme: ~/.cache/wal/colors-rofi-light.rasi
```

### polybar

First we'll need to install `polybar`:

```
yay --aur -S polybar
```

You should already have all of the config files set up after running the `stow_all`
script so polybar should work right after reloading i3. If something seems wonky or it
doesn't work at all check to see if the `stow_all` script worked properly.

### Installing fonts

## System stuff

### Set some defaults for `yay`

The easiest way to go about this is using the `--save` option with `yay`:

```
yay --save --nodiffmenu --noeditmenu
```

This will create a json file in `~/.config/yay` where the corresponding options are set.
From this file, you can customize all of `yay`'s options.

### Creating `pacman` hooks for `paccache`

Before we start, check to make sure that the package `pacman-contrib` has been installed
on your system.

```
pacman -Q pacman-contrib
```

If the command returns an error the you'll need to download the package. Once this is done
create a `hooks` directory in `/etc/pacmand.d` and add the following two files:
`clean-package-cache.hook` and `remove-uninstalled-packages.hook`. In
`clean-package-cache.hook`, add the following lines:

```
[Trigger]
Operation = Upgrade
Operation = Install
Operation = Remove
Type = Package
Target = *

[Action]
Description = Removing all unused packages from the cache...
When = PostTransaction
Exec = /usr/bin/pacacche -ruk0
```

and in `remove-uninstalled-packages.hook` add:

```
[Trigger]
Operation = Upgrade
Operation = Install
Operation = Remove
Type = Package
Target = *

[Action]
Description = Removing all but the last three versions of all installed packages from the cache...
When = PostTransaction
Exec = /usr/bin/pacacche -r
```

The next time you install, upgrade, or remove packages using `pacman`, the `paccache`
utility should be run with the specified options. You can substitute `paccache` with
another utility or your own script.

## Programs and other tools

### Installing and setting up `mpd` and `ncmpcpp`

Start by installing both of these programs

```
sudo pacman -S mpd ncmpcpp
```

#### `mpd` setup

Once the installation is complete create a directory for your `mpd` files. I've put mine
in `~/.config/mpd` but you can use `~/.mpd` if you'd like. Create all of the necessary
files in this directory:

```
touch ~/.config/mpd/mpd.{db,log,pid}
```

Copy the example config file here as well:

```
# cp /usr/share/doc/mpd/mpd.example ~/.config/mpd/mpd.conf
```

In the `mdp.conf` file, comment out the code so that it reads as follows (you may actually
opt to delete all of the uneccessary commented out code):

```
music_directory "/path/to/music/dir"
db_file "~/.config/mpd/mpd.db"
log_file "~/.config/mpd/mpd.log"
pid_file "~/.config/mpd/mpd.pid"
user "<your_username>"
bind_to_address "127.0.0.1"
port "6600"
auto_update "yes"

audio_output {
        type "pulse"
        output "mpd output"
}

audio_output {
        type "fifo"
        output "my_fifo"
        path "/tmp/mpd.fifo"
        format "44100:16:2"
}
```

Then create a directory called `mpd.service.d` in `/etc/systemd/system/` and add your user
configs there

```
# mkdir /etc/systemd/system/mpd.service.d
# touch user.conf
```

In the `user.conf` file add the following lines:

```
[Service]
User=<your_username>
```

This allows the `mpd` service to run in "per-user mode".

#### `ncmpcpp` setup

Create a directory to store the `ncmpcpp` config files and then copy the example config
file to it:

```
mkdir ~/.config/ncmpcpp
# cp /usr/share/doc/ncmpcpp/config ~/.config/ncmpcpp/
```

Then in the config file uncomment the following lines and edit them as shown below:

```
mpd_music_dir = "/path/to/music/dir"
display_bitrate = "yes"
visualizer_fifo_path = "/tmp/mpd.fifo"
visualizer_output_name = "my_fifo"
visualizer_sync_interval = "30"
visualizer_in_stereo = "yes"
visualizer_type = "spectrum"
visualizer_look = "+|"
```

Enable and start the `mpd` service

```
sudo systemctl enable mpd
sudo systemctl start mpd
```

You can now run `ncmpcpp`

### Installing and setting up `dunst`

In order to see system and program notifications we'll install `dunst`:

```
sudo pacman -S dunst
```

Create the `dunst` directory in `~/.config` if it doesn't already exist and then copy the
example `dunstrc` file to this directory:

```
mkdir ~/.config/dunst
sudo cp /usr/share/dunstrc ~/.config/dunst
```

For better notification positioning change the `geometry` as follows

```
geometry = "300x5-20+40"
```

In the event that you change the size of the gaps between windows in your i3 config you
can always test the size of the `dunst` notification windows with the following commands:

```
killall dunst
notify-send "Test"
```

You should see a dunst notification pop up with any changes that you've made to it. Repeat
the above commands until you're satisfied with the look and position of the `dunst`
notifications.

We'll also setup dunst to match our current pywal colorscheme. Open the file `color.sh`
from the pywal cache located in `~/.cache/wal` in your favorite text editor. You'll also
want to have the `wal.json` file open as well. Then, look
for the blocks of code below the following headers:
- urgency_low
- urgency_normal
- urgency_critical
Set the background color for each block to the color assigned to the background variable.
Then set both the text and foreground color to the colors used for the "good",
"foreground", and "critical" variables in the `wal.json` file.

### Configuring ranger

#### Enabling dev icons

At this point you should have already installed NerdFonts. If you're still unsure, run
`fc-list | rg 'nerd' | cat`. This should spit out a list of all of the installed
NerdFonts. If nothing is returned, you'll need to revisit the earlier section on
installing fonts. Enabling dev icons is as simple as cloning the repo:

```
git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons
```

And then adding the following to your `rc.conf` file:

```
default_linemode devicons
```

#### Enabling syntax highlighting of code previews

Simply install the `highlight` package:

```
sudo pacman -S highlight
```

You can configure the syntax theme in the `scope.sh` file. Just change the value of the
`HIGHLIGHT_STYLE` variable.
