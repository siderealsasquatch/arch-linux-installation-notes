# Resources list

This is a list of the resources that I used when writing my notes. Not sure yet if I want
to leave this list in the repository or combine it with my notes. For the time being the
list stays.

_NOTE:_ This is just a reminder to myself to cross reference all of these articles and
posts before I begin the installation.

## Partition schemes for EFI
- [Take a look at the first answer of this question posted on Quora][1]

## EFI troubleshooting
- [Consult this thread in the Arch forums if things go pear shaped][2]

## Dual booting tips and instructions
- [Official Arch dual boot guide][3]
- [Guide for setting up EFI][4]
- [Manjaro EFI guide][5]
- [EFI pre-installation steps][6]

## Full Arch Linux installation instructions

### Installation
- [Official Arch guide][7]
- [Glorious Eggroll's guide with video][8]
- [A guide in Indonesian by Emira Linuxer][9]
- [Detailed guide with videos covering each step][10]
- [TecMint's Arch Linux installation guide][11]
- [Gist with cookbook-style instructions on how to install Arch Linux with UEFI][12]
- [Concise installation instructions with commands][13]
- [Full guide by Swapnil][14]
- [Beginner's guide. Good for cross-referencing with the other guides here][15]

### Post installation

## i3 setup and customization
- [Really great detailed guide on setting up i3 and making it usable][16]
- [Similar to the guide above. Even uses similar utilities and programs][17]
- [This is more of a config guide but still good][18]
- [Guide on ricing your system with i3. Might not be needed but including it anyway][19]
- [i3 dotfile "repo" of sorts. Take a look at a few of them][20]
- [Some i3 tips. Goes over additional packages that should be installed with i3][21]
- [Detailed setup guide. For Manjaro but can be modified to work with Arch][22]

## i3 and general troubleshooting

- [Some general config tips][23]
- [More config tips][24]
- [i3 with multi-monitor setup (for future reference)][25]

### Networking
- [Connecting to a network with NetworkManager][26]
- [Automatically connecting to network on startup][27]
- [Another (short) guide to connecting to a network on startup][28]

### Displays
- [Configuring displays for i3][29]

### Automount external drives/USBs

- [Small utility for automounting external devices][30]

[1]: https://www.quora.com/Can-I-dual-boot-Linux-and-Windows-10-with-Windows-10-as-a-primary-How
[2]: https://bbs.archlinux.org/viewtopic.php?id=225126
[3]: https://wiki.archlinux.org/index.php/Dual_boot_with_Windows
[4]: http://www.rodsbooks.com/linux-uefi/
[5]: https://forum.manjaro.org/t/wiki-windows-10-manjaro-dual-boot-step-by-step/52668
[6]: https://www.zdnet.com/article/installing-linux-on-a-pc-with-uefi-firmware-a-refresher/
[7]: https://wiki.archlinux.org/index.php/Installation_guide
[8]: https://www.gloriouseggroll.tv/arch-linux-efi-install-guide/
[9]: https://www.emirar.com/2018/03/instal-arch-linux-uefi.html
[10]: https://www.addictivetips.com/ubuntu-linux-tips/how-to-install-arch-linux/
[11]: https://www.tecmint.com/arch-linux-installation-and-configuration-guide/
[12]: https://gist.github.com/mattiaslundberg/8620837
[13]: https://nnserverthings.wordpress.com/2017/07/04/arch-linux-with-windows-10-dual-boot/
[14]: https://www.tfir.io/install-arch-linux-like-pro/
[15]: https://linoxide.com/distros/beginners-arch-linux-installation-guide/
[16]: http://briancaffey.github.io/2017/10/17/moving-from-gnome-to-i3-on-arch-linux.html
[17]: https://github.com/ibrahimbutt/direwolf-arch-rice
[18]: https://confluence.jaytaala.com/display/TKB/My+Manjaro+i3+setup
[19]: https://sinister.ly/Thread-Tutorial-The-Lazy-Man-s-Guide-to-Ricing-Linux-WM-i3
[20]: http://dotshare.it/category/wms/i3/
[21]: https://www.zdnet.com/article/hands-on-with-the-i3-window-manager-installing-configuring-and-using/
[22]: https://confluence.jaytaala.com/display/TKB/My+Manjaro+i3+setup
[23]: https://vickylai.com/verbose/how-a-lifelong-windows-user-switched-to-linux-the-hard-way/
[24]: https://extendedreality.wordpress.com/2014/11/20/i3-tips-n-tricks/
[25]: http://neymarsabin.com/i3-and-multiple-monitors
[26]: http://briancaffey.github.io/2017/10/20/how-to-connect-to-starbucks-wifi-with-i3.html
[27]: http://edunham.net/2015/02/05/making_arch_do_the_right_thing_at_startup.html
[28]: https://www.hitbits.net/2017/04/08/wifi-gui-arch-linux-i3wm/
[29]: https://christopherdecoster.com/posts/i3-wm/
[30]: https://www.reddit.com/r/i3wm/comments/2f8ytn/automount/
